﻿using juwedotnet.Model.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace juwedotnet.Persistence.Services
{
    public interface IRepository
    {
        void AddUser(string nick);
        bool SaveAll();
        User GetUser(int id);
        void CreateToken(string name);
    }
}
