﻿using juwedotnet.Model.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace juwedotnet.Persistence.Services
{
    public class Repository:IRepository
    {
        private readonly AppDbContext _appDbContext;
        public Repository(AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;
        }

        public bool SaveAll()
        {
            return _appDbContext.SaveChanges() > 0;
        }

        public void AddUser(string nick)
        {
            var user = new User();
            user.Nick = nick;
            _appDbContext.Users.Add(user);
        }

        public User GetUser(int id)
        {
            return _appDbContext.Users.FirstOrDefault(u => u.Id == id);
        }

        public void CreateToken(string name)
        {
            _appDbContext.Tokens.Add(new Token() { Name = name });
        }
    }
}
