﻿using juwedotnet.Model.Models;
using juwedotnet.Model.ViewModels;
using juwedotnet.Persistence.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace juwedotnet.WebApi.Controllers
{
    [Route("api/[controller]")]
    public class UsersController:Controller
    {
        private readonly IRepository _repository;

        public UsersController(IRepository repository)
        {
            _repository = repository;
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var user = _repository.GetUser(id);
            if (user != null)
                return Ok(user);
            else return BadRequest();
        }

        [HttpPost]
        public IActionResult Create([FromBody] UserViewModel model)
        {
            if (ModelState.IsValid)
            {
                var newUser = new User { Nick = model.Nick };

                _repository.AddUser(model.Nick);
                if (_repository.SaveAll())
                    return Created($"/api/user/", newUser);

            }
            return BadRequest(ModelState);

        }
    }
}
