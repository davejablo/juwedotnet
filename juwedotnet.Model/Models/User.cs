﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace juwedotnet.Model.Models
{
    public class User
    {
        public int Id { get; set; }
        [Required]
        [MaxLength(20)]
        public string Nick { get; set; }
        public int Points { get; set; }
        public List<UserToken> UserTokens { get; set; }
    }
}
