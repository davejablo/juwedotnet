﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace juwedotnet.Model.Models
{
    public class UserToken
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int TokenId { get; set; }
        [ForeignKey("TokenId")]
        public Token Token { get; set; }
        public DateTime Date { get; set; }
    }
}
