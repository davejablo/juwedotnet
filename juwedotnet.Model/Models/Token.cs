﻿using System;
using System.Collections.Generic;
using System.Text;

namespace juwedotnet.Model.Models
{
    public class Token
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Guid Code { get; set; }
        public int Points { get; set; }
        public bool IsMultiple { get; set; }
    }
}
