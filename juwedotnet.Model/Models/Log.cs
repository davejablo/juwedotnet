﻿using System;
using System.Collections.Generic;
using System.Text;

namespace juwedotnet.Model.Models
{
    public class Log
    {
        public int Id { get; set; }
        public string Nick { get; set; }
        public int TokenId { get; set; }
        public DateTime Date { get; set; }
        public enum Type { fight, tokenfinder }
    }
}
